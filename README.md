# jwks-transform

JWKS Transform will take a URL to a JWKS endpoint and generate the formatted
secret that Talk expects.

```sh
./jwks-transform -url https://<my-url>
```
