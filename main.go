package main

import (
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/coralproject/coralcert/secret"
	jose "gopkg.in/square/go-jose.v2"
)

func main() {
	url := flag.String("url", "", "specify the URL to the JWKS endpoint")

	flag.Parse()

	if *url == "" {
		log.Fatalln("-url is required")
	}

	// Load the JWKS URL.
	res, err := http.Get(*url)
	if err != nil {
		log.Fatalln(err)
	}
	defer res.Body.Close()

	// Decode the JWKS.
	var jwks jose.JSONWebKeySet
	if err := json.NewDecoder(res.Body).Decode(&jwks); err != nil {
		log.Fatalln(err)
	}

	for _, key := range jwks.Keys {
		for _, cert := range key.Certificates {
			// Encode the public key to a DER-encoded PKIX format.
			publicKeyDER, err := x509.MarshalPKIXPublicKey(cert.PublicKey)
			if err != nil {
				log.Fatalln(err)
			}

			// Prepare the DER-encoded PKIX encoded form in a PEM block.
			publicKeyPEM := &pem.Block{
				Type:  "PUBLIC KEY",
				Bytes: publicKeyDER,
			}

			s, err := secret.Marshal(key.KeyID, publicKeyPEM, nil)
			if err != nil {
				log.Fatalln(err)
			}

			fmt.Println(string(s))
		}
	}
}
